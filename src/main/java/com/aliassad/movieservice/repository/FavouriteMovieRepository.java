package com.aliassad.movieservice.repository;

import com.aliassad.movieservice.domain.FavouriteMovie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FavouriteMovieRepository extends JpaRepository<FavouriteMovie,Long> {


    List<FavouriteMovie> findAllByUserId(Long userId);

    void deleteFavouriteMovieByUserIdAndMovieId(Long userId,Long movieId);
}
