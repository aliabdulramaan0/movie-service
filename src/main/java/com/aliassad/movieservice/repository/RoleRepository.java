package com.aliassad.movieservice.repository;

import com.aliassad.movieservice.domain.Role;

import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Long>{

}
