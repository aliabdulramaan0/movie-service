package com.aliassad.movieservice.service;

import com.aliassad.movieservice.domain.Movie;
import com.aliassad.movieservice.domain.Person;
import com.aliassad.movieservice.service.dto.PersonListDTO;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;

@Service
public class PersonService {

   @Value("${api.key}")
   String apiKey;

   @Value("${url}")
   String url;

   private final RestTemplate restTemplate;

   public PersonService(RestTemplate restTemplate) {
      this.restTemplate = restTemplate;
   }

   public PersonListDTO findAllCastByMovieId(Long id){

     HttpHeaders httpHeaders = new HttpHeaders();
     httpHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
     HttpEntity <String> entity= new HttpEntity<>(httpHeaders);

     ResponseEntity<PersonListDTO>response=restTemplate.exchange(url + "movie/"+id.toString()+"/credits?api_key=" + apiKey + "&language=en-US", HttpMethod.GET, entity, new ParameterizedTypeReference<PersonListDTO>() {
     });
     return response.getBody();
   }



    public Person findPersonById(Long id){

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity <String> entity= new HttpEntity<>(httpHeaders);

        ResponseEntity<Person>response=restTemplate.exchange(url + "person/"+id.toString()+"?api_key=" + apiKey + "&language=en-US", HttpMethod.GET, entity, new ParameterizedTypeReference<Person>() {
        });
        return response.getBody();
    }


}
