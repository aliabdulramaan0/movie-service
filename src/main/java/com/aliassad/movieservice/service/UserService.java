package com.aliassad.movieservice.service;

import com.aliassad.movieservice.domain.User;
import com.aliassad.movieservice.repository.UserRepository;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserService {

    private final UserRepository userRepository;
  //  private final BCryptPasswordEncoder cryptPasswordEncoder;
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
       // this.cryptPasswordEncoder = cryptPasswordEncoder;
    }


    public User save(User user){
//String encryptPassword=cryptPasswordEncoder.encode(user.getPassword());
//user.setPassword(encryptPassword);
        User result=userRepository.save(user);
        return result;


    }


    public User findUserByUserLogin(String userName){

        Optional<User>result= userRepository.findByUsername(userName);
        if(!result.isPresent()){

            throw new UsernameNotFoundException("User does not exsist with this name : " + userName);

        }

       return result.get();

    }

}
