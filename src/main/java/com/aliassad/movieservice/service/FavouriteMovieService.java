package com.aliassad.movieservice.service;


import com.aliassad.movieservice.domain.FavouriteMovie;
import com.aliassad.movieservice.domain.User;
import com.aliassad.movieservice.repository.FavouriteMovieRepository;
import com.aliassad.movieservice.service.dto.UserDetailsImp;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class FavouriteMovieService implements Serializable {
    private final Logger log = LoggerFactory.getLogger(FavouriteMovieService.class);

    private final FavouriteMovieRepository favouriteMovieRepository;
    private final UserService userService;
    public FavouriteMovieService(FavouriteMovieRepository favouriteMovieRepository, UserService userService) {
        this.favouriteMovieRepository = favouriteMovieRepository;
        this.userService = userService;
    }


    public void addFavouriteMovieToUser(Long movieId){

//        log.debug("request to save favourite movie");
//
//        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
//        UserDetailsImp currentPrincipalName = (UserDetailsImp)authentication.getPrincipal();
//
//        User user=userService.findUserByUserLogin(currentPrincipalName.getUser().getUsername());
//        if(Objects.nonNull(user)){
//            FavouriteMovie favouriteMovie=new FavouriteMovie(movieId,user.getId());
//            favouriteMovieRepository.save(favouriteMovie);
//        }





    }


    @Transactional(readOnly = true)

    public List<Long> findMoviesIdByUserId(Long userId){
        log.debug("request to get all favourite movies by"+userId);

        List<FavouriteMovie>result=favouriteMovieRepository.findAllByUserId(userId);

        List<Long>moviesId=result.stream().map(FavouriteMovie::getMovieId).collect(Collectors.toList());
        return moviesId;
    }


    public void delete(Long id){
        log.debug("request to delete favourite movies by"+id);

       favouriteMovieRepository.deleteById(id);

    }
//    @Transactional
//    public void deleteByUserIdAndMovieId(Long movieId){
//        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
//        UserDetailsImp currentPrincipalName = (UserDetailsImp)authentication.getPrincipal();
//
//        User user=userService.findUserByUserLogin(currentPrincipalName.getUser().getUsername());
//        if(Objects.nonNull(user)){
//            favouriteMovieRepository.deleteFavouriteMovieByUserIdAndMovieId(user.getId(), movieId);
//        }
//
//
//
//    }

}
