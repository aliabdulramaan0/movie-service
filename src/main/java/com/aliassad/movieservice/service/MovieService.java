package com.aliassad.movieservice.service;


import com.aliassad.movieservice.domain.Movie;
import com.aliassad.movieservice.domain.User;
import com.aliassad.movieservice.service.dto.MovieDetailsDTO;
import com.aliassad.movieservice.service.dto.MovieListDTO;
import com.aliassad.movieservice.service.dto.PersonListDTO;
import com.aliassad.movieservice.service.dto.UserDetailsImp;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import java.util.*;

@Service
public class MovieService {

    @Value("${api.key}")
    private String apiKey;

    @Value("${url}")
    private String url;

    private final RestTemplate restTemplate;
    private final PersonService personService;
    private final UserService userService;
    private final FavouriteMovieService favouriteMovieService;

    public MovieService(RestTemplate restTemplate, PersonService personService, UserService userService, FavouriteMovieService favouriteMovieService) {
        this.restTemplate = restTemplate;
        this.personService = personService;
        this.userService = userService;
        this.favouriteMovieService = favouriteMovieService;
    }


    public MovieDetailsDTO findMovieDetailsById(Long id) {

        MovieDetailsDTO movieDetailsDTO = new MovieDetailsDTO();
        Movie movie = findMovieById(id);
        if (Objects.nonNull(movie)) {
            movieDetailsDTO.setMovie(movie);
        }

        PersonListDTO personListDTO = personService.findAllCastByMovieId(id);
        if (Objects.nonNull(personListDTO)&&Objects.nonNull(personListDTO.getCast())&&(!personListDTO.getCast().isEmpty())) {
            movieDetailsDTO.setPersonList(personListDTO.getCast());
        }
        List<Movie> similarMovie = findSimilarMovieById(id);
        if(Objects.nonNull(similarMovie)&&(!similarMovie.isEmpty())){
            movieDetailsDTO.setSimilarMovie(similarMovie);
        }
        return movieDetailsDTO;

    }

    public List<Movie> findLatestMovie() {


        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity<String> entity = new HttpEntity<String>(headers);
        ResponseEntity<Movie> response = restTemplate.exchange(url + "movie/latest?api_key=" + apiKey + "&language=en-US", HttpMethod.GET, entity, new ParameterizedTypeReference<Movie>() {
        });

        response.getBody().setPoster_path("/xZburE7oPN0mMlD6HTkpTHO8ARR.jpg");

        return new ArrayList<>(Arrays.asList(response.getBody()));

    }


    public  List<Movie> findFavouriteMovieByUserId() {
        List<Movie> favouriteMovies = new ArrayList<>();

//        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
//        UserDetailsImp currentPrincipalName = (UserDetailsImp) authentication.getPrincipal();
//
//        User user = userService.findUserByUserLogin(currentPrincipalName.getUser().getUsername());
//        if (Objects.nonNull(user)) {
//            List<Long> moviesId = favouriteMovieService.findMoviesIdByUserId(user.getId());
//            moviesId.forEach(id -> {
//                Movie movie = findMovieById(id);
//                if (Objects.nonNull(movie)) {
//                    favouriteMovies.add(movie);
//                }
//
//            });
//        }

        return favouriteMovies;




    }





    public List<Movie> findFavouriteMovieByUser() {
        List<Movie> favouriteMovies = new ArrayList<>();
//        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
//        UserDetailsImp currentPrincipalName = (UserDetailsImp) authentication.getPrincipal();
//
//        User user = userService.findUserByUserLogin(currentPrincipalName.getUser().getUsername());
//        if (Objects.nonNull(user)) {
//            List<Long> moviesId = favouriteMovieService.findMoviesIdByUserId(user.getId());
//            moviesId.forEach(id -> {
//                Movie movie = findMovieById(id);
//                if (Objects.nonNull(movie)) {
//                    favouriteMovies.add(movie);
//                }
//
//            });
//        }



        return favouriteMovies;




    }

    public Movie findMovieById(Long id) {

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity<String> entity = new HttpEntity<String>(headers);
        ResponseEntity<Movie> response = restTemplate.exchange(url + "movie/" + id.toString() + "?api_key=" + apiKey + "&language=en-US", HttpMethod.GET, entity, new ParameterizedTypeReference<Movie>() {
        });
        return response.getBody();

    }

    public List<Movie> findSimilarMovieById(Long id) {

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity<String> entity = new HttpEntity<String>(headers);
        ResponseEntity<MovieListDTO> response = restTemplate.exchange(url + "movie/" + id.toString() + "/similar" + "?api_key=" + apiKey + "&language=en-US", HttpMethod.GET, entity, new ParameterizedTypeReference<MovieListDTO>() {
        });
        return response.getBody().getResults();

    }


//    public ModelAndView findPopularMovie(Integer page) {
//
//Map<String,Object>map=new HashMap<>();
//
//        if (Objects.isNull(page)) {
//            page = 1;
//        }
//
//        HttpHeaders headers = new HttpHeaders();
//        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
//
//        HttpEntity<String> entity = new HttpEntity<String>(headers);
//        ResponseEntity<MovieListDTO> response = restTemplate.exchange(url + "movie/popular?api_key=" + apiKey + "&language=en-US+&page=" + page, HttpMethod.GET, entity, new ParameterizedTypeReference<MovieListDTO>() {
//        });
//
//        List<Movie> movies=new ArrayList<>();
//        List<Movie> popularMovies = response.getBody().getResults();
//        List<Movie> favouriteMovies = findFavouriteMovieByUser();
//
//        popularMovies.removeAll(favouriteMovies);
//        popularMovies.stream().forEach(movie -> movie.setFavourite(false));
//        favouriteMovies.stream().forEach(movie -> movie.setFavourite(true));
//        movies.addAll(popularMovies);
//        movies.addAll(favouriteMovies);
//
//
////        Integer pageSize = response.getBody().getTotal_pages();
////        Integer currentPage=response.getBody().getPage();
////
////
////        Page<Movie> moviePage
////                = new PageImpl<Movie>(movies, PageRequest.of(currentPage, pageSize), movies.size());
//       ModelAndView modelAndView = new ModelAndView();
//       modelAndView.setViewName("movie");
//
//
//        map.put("currentPage", response.getBody().getPage());
//        map.put("totalPages", response.getBody().getTotal_pages());
//        map.put("totalItems", response.getBody().getTotal_results());
//        map.put("movies", movies);
//
//
//
//
////        map.put("moviePage",moviePage);
////
////        if (pageSize > 0) {
////            List<Integer> pageNumbers = IntStream.rangeClosed(1, pageSize)
////                    .boxed()
////                    .collect(Collectors.toList());
////           map.put("pageNumbers",pageNumbers);
////        }
//
//    //  modelAndView.addObject("movies", movies);
//       modelAndView.addAllObjects(map);
//        return modelAndView;
//
//
//
//
//
//    }





    public  List<Movie> findPopularMovie(Integer page) {


        if (Objects.isNull(page)) {
            page = 1;
        }

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

        HttpEntity<String> entity = new HttpEntity<String>(headers);
        ResponseEntity<MovieListDTO> response = restTemplate.exchange(url + "movie/popular?api_key=" + apiKey + "&language=en-US+&page=" + page, HttpMethod.GET, entity, new ParameterizedTypeReference<MovieListDTO>() {
        });


        List<Movie> popularMovies = response.getBody().getResults();

//Map<String,Object>map=new HashMap<>();
// List<Movie> movies=new ArrayList<>();
// List<Movie> favouriteMovies = findFavouriteMovieByUser();
//    popularMovies.removeAll(favouriteMovies);
//    popularMovies.stream().forEach(movie -> movie.setFavourite(false));
//  favouriteMovies.stream().forEach(movie -> movie.setFavourite(true));
//   movies.addAll(popularMovies);
//   movies.addAll(favouriteMovies);
//        Integer pageSize = response.getBody().getTotal_pages();
//        Integer currentPage=response.getBody().getPage();
//        Page<Movie> moviePage
//                = new PageImpl<Movie>(movies, PageRequest.of(currentPage, pageSize), movies.size());
//       ModelAndView modelAndView = new ModelAndView();
//       modelAndView.setViewName("moviiie");
//        map.put("currentPage", page);
//        map.put("totalPages", response.getBody().getTotal_pages());
//        map.put("totalItems", response.getBody().getTotal_results());
//        map.put("movies", movies);
//        map.put("moviePage",moviePage);
//        if (pageSize > 0) {
//            List<Integer> pageNumbers = IntStream.rangeClosed(1, pageSize)
//                    .boxed()
//                    .collect(Collectors.toList());
//           map.put("pageNumbers",pageNumbers);
//        }
//modelAndView.addObject("movies",movies);
//   modelAndView.addAllObjects(map);


        return  popularMovies;





    }



    public List<Movie> searchMovie(String query) {
        List<Movie> movies=new ArrayList<>();
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity<String> entity = new HttpEntity<String>(headers);
        ResponseEntity<MovieListDTO> response = restTemplate.exchange(url + "search/movie?api_key=" + apiKey + "&language=en-US&query=" + query, HttpMethod.GET, entity, new ParameterizedTypeReference<MovieListDTO>() {
        });
        List<Movie> popularMovies = response.getBody().getResults();



        return response.getBody().getResults();

    }




}
