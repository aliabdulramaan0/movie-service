package com.aliassad.movieservice.service.dto;

import com.aliassad.movieservice.domain.Movie;

import java.io.Serializable;

public class MovieDTO extends Movie implements Serializable {

    Long id;
    Boolean favourite;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean isFavourite() {
        return favourite;
    }

    public void setFavourite(Boolean favourite) {
        this.favourite = favourite;
    }
}
