package com.aliassad.movieservice.service.dto;

import com.aliassad.movieservice.domain.Movie;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class MovieListDTO implements Serializable {
    private Long id;
    Integer page;
    Integer total_pages;
    Integer total_results;
    List<Movie> results= new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<Movie> getResults() {
        return results;
    }

    public void setResults(List<Movie> results) {
        this.results = results;
    }


    public Integer getTotal_pages() {
        return total_pages;
    }

    public void setTotal_pages(Integer total_pages) {
        this.total_pages = total_pages;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getTotal_results() {
        return total_results;
    }

    public void setTotal_results(Integer total_results) {
        this.total_results = total_results;
    }
}
