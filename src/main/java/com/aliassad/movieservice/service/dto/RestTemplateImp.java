package com.aliassad.movieservice.service.dto;

import com.aliassad.movieservice.domain.Movie;

import java.util.ArrayList;
import java.util.List;

public class RestTemplateImp {

    Integer page;
    Integer total_pages;
    List<Movie>results=new ArrayList<>();


    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getTotal_pages() {
        return total_pages;
    }

    public void setTotal_pages(Integer total_pages) {
        this.total_pages = total_pages;
    }

    public List<Movie> getResults() {
        return results;
    }

    public void setResults(List<Movie> results) {
        this.results = results;
    }
}
