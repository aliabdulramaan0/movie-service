package com.aliassad.movieservice.service.dto;

import com.aliassad.movieservice.domain.Person;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class PersonListDTO implements Serializable {

    Long id;
    List<Person> cast= new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<Person> getCast() {
        return cast;
    }

    public void setCast(List<Person> cast) {
        this.cast = cast;
    }
}
