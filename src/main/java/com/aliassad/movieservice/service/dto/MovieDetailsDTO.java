package com.aliassad.movieservice.service.dto;

import com.aliassad.movieservice.domain.Genre;
import com.aliassad.movieservice.domain.Movie;
import com.aliassad.movieservice.domain.Person;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class MovieDetailsDTO implements Serializable {

    Movie movie;
    List<Person> personList= new ArrayList<>();
    List<Movie> similarMovie= new ArrayList<>();


    public Movie getMovie() {
        return movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }

    public List<Person> getPersonList() {
        return personList;
    }

    public void setPersonList(List<Person> personList) {
        this.personList = personList;
    }

    public List<Movie> getSimilarMovie() {
        return similarMovie;
    }

    public void setSimilarMovie(List<Movie> similarMovie) {
        this.similarMovie = similarMovie;
    }


}
