package com.aliassad.movieservice.resource;


import com.aliassad.movieservice.domain.Movie;
import com.aliassad.movieservice.service.MovieService;
import com.aliassad.movieservice.service.dto.MovieDetailsDTO;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.*;

@RestController
@RequestMapping("/api")
public class MovieResource {
    private final MovieService movieService;

    public MovieResource(MovieService movieService) {
        this.movieService = movieService;
    }




    @GetMapping("/movies/details/{id}")
    public MovieDetailsDTO getMovieDetails(@PathVariable Long id) {
        MovieDetailsDTO movie = movieService.findMovieDetailsById(id);

        return movie;

    }

    @GetMapping("/movies/favourite")
    public   List<Movie> getFavouriteMovie() {
        List<Movie> movies = movieService.findFavouriteMovieByUserId();

        return movies;
    }


    @GetMapping("/movies/latest")
    public  List<Movie> getLatestMovies() {
        List<Movie> movies = movieService.findLatestMovie();

        return movies;
    }




    @GetMapping("/movies/popular/{page}")
    public  List<Movie> getPopularMovies(@PathVariable Integer page) {
        List<Movie> movies=movieService.findPopularMovie(page);

        return movies;
    }



    @GetMapping("/search/movies")
    public List<Movie> searchMovies(@RequestParam(value = "query" ) String query) {
        List<Movie> movies = movieService.searchMovie(query.toLowerCase());



        return movies;
    }



}
