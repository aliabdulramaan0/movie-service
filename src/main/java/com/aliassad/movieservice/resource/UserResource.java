package com.aliassad.movieservice.resource;

import com.aliassad.movieservice.domain.User;
import com.aliassad.movieservice.service.UserService;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.Serializable;

@RestController
@RequestMapping("/api")
public class UserResource implements Serializable {

    private final UserService userService;

    public UserResource(UserService userService) {
        this.userService = userService;
    }


//    private final AuthenticationManager authenticationManager;
//
//
//    private final JwtUtil jwtTokenUtil;
//
//
//    private final UserDetailsServiceImp userDetailsService;
//
//
//    public UserResource(UserService userService, AuthenticationManager authenticationManager, JwtUtil jwtTokenUtil, UserDetailsServiceImp userDetailsService) {
//        this.userService = userService;
//        this.authenticationManager = authenticationManager;
//        this.jwtTokenUtil = jwtTokenUtil;
//        this.userDetailsService = userDetailsService;
//    }
//
//    @PostMapping("/authenticate")
//    public ResponseEntity<?> createAuthenticationToken(@RequestBody AuthenticationRequest authenticationRequest) throws Exception {
//
//        authenticate(authenticationRequest.getUserName(), authenticationRequest.getPassword());
//
//        final UserDetails userDetails = userDetailsService
//                .loadUserByUsername(authenticationRequest.getUserName());
//
//        final String token = jwtTokenUtil.generateToken(userDetails);
//
//        return ResponseEntity.ok(new AuthenticationResponse(token));
//    }
//
//    private void authenticate(String username, String password) throws Exception {
//        try {
//            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
//        } catch (DisabledException e) {
//            throw new Exception("USER_DISABLED", e);
//        } catch (BadCredentialsException e) {
//            throw new Exception("INVALID_CREDENTIALS", e);
//        }
//    }
//
//
//
//











    @PostMapping("/users")
    public User createUser(@RequestBody User user){

    User result=userService.save(user);
    return result;

}


    @GetMapping("/users/get-by-username/{userName}")
    public User getUserByUserName(@PathVariable String userName){

        User result=userService.findUserByUserLogin(userName);
        return result;

    }


//    @GetMapping("/users/logging-user")
//    public ModelAndView getUserByUserName(){
//
//        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
//        UserDetailsImp currentPrincipalName = (UserDetailsImp)authentication.getPrincipal();
//        ModelAndView modelAndView = new ModelAndView();
//        modelAndView.setViewName("movie");
//        modelAndView.addObject("user", currentPrincipalName.getUser());
//        return modelAndView;
//    }
    @GetMapping("/logout")
    public String logoutPage (HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null){
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }
        return "redirect:/login?logout";
    }

}
