package com.aliassad.movieservice.resource;

import com.aliassad.movieservice.domain.Movie;
import com.aliassad.movieservice.service.FavouriteMovieService;
import com.aliassad.movieservice.service.MovieService;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/api")
public class FavouriteMovieResource {


    private final FavouriteMovieService favouriteMovieService;
    private final MovieService movieService;
    public FavouriteMovieResource(FavouriteMovieService favouriteMovieService, MovieService movieService) {
        this.favouriteMovieService = favouriteMovieService;
        this.movieService = movieService;
    }

    @PostMapping("/favourite-movie/{movieId}")
    public List<Movie> addFavouriteMovie(@PathVariable Long movieId) throws URISyntaxException {

       favouriteMovieService.addFavouriteMovieToUser(movieId);
        List<Movie> movies = movieService.findFavouriteMovieByUserId();
        return movies;
    }


//    @PostMapping("/remove-favourite-movie/{movieId}")
//    public void deleteFavouriteMovie(@PathVariable Long movieId) throws URISyntaxException {
//
//        favouriteMovieService.deleteByUserIdAndMovieId(movieId);
//
//
//    }



}
