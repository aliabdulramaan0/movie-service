package com.aliassad.movieservice.resource;


import com.aliassad.movieservice.domain.Person;
import com.aliassad.movieservice.service.PersonService;
import com.aliassad.movieservice.service.dto.PersonListDTO;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class PersonResource {

    private final PersonService personService;

    public PersonResource(PersonService personService) {
        this.personService = personService;
    }

    @GetMapping("/peoples/by-movie-id/{id}")
    public PersonListDTO getAllCastByMovieId(@PathVariable Long id) {
        PersonListDTO personListDTO = personService.findAllCastByMovieId(id);

        return personListDTO;
    }


    @GetMapping("/peoples/{id}")
    public Person getById(@PathVariable Long id) {

        Person person = personService.findPersonById(id);

        return person;
    }





}
